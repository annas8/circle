const circles = document.querySelector('.circles');

const drawCircles = () => {
      let d = prompt('Вкажіть діаметр кругів (до 100 пікселів)', 50);
      createCircles(d);
}

const createCircles = (d) => {
    let template = '';
    for (let i = 0; i < 100; i++) {
        let circleColor = `rgb(${String(Math.floor(Math.random() * 256))}, ${String(Math.floor(Math.random() * 256))},${String(Math.floor(Math.random() * 256))})`;
        let circle = `<div class="circle" style="width: ${d}px; height: ${d}px; background-color: ${circleColor};"></div>`;
        template += circle;
    }
    circles.innerHTML = template;
}
circles.addEventListener('click', ({target}) => {
    if(target.matches('.circle')) {
        target.style.display = 'none';
    }
})
